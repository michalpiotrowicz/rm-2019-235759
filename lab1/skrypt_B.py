import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import random as rnd
from matplotlib.path import Path
from matplotlib.patches import PathPatch

delta = 0.1
x_size = 10
y_size = 10
start_y=rnd.randint(-10,10)
#losowanie punktu poczatkowego
start_point=(-10,start_y)
finish_y=rnd.randint(-10,10)
#losowanie punktu koncowego
finish_point=(10,finish_y)
#losowanie wspolrzednych 4 przeszkod
obst_vect = [(rnd.randint(-10,10), rnd.randint(-10,10)), (rnd.randint(-10,10), rnd.randint(-10,10)),
			 (rnd.randint(-10,10), rnd.randint(-10,10)), (rnd.randint(-10,10), rnd.randint(-10,10))]

#funkcja obliczajaca odleglosc miedzy punktami q i qk
def qDist(q, qk):
	return np.sqrt((q[0] - qk[0])**2 + (q[1] - qk[1])**2)

#funkcja opisujaca potencjal przyciagajacy robota w punkcie q
def Up(q, qk, kp):
	return 0.5*kp*(qDist(q, qk)**2)

#funkcja opisujaca potencjal odpychakjacy od i-tej przeszkody
def Voi(q, qoi, koi, do):
	if qDist(q, qoi) > do:
		return 0
	return 0.5*(koi*(1/qDist(q, qoi) - 1/do)**2)

#funkcja opisujaca sile przyciagajaca
def Fp(q, qk, kp):
	return kp*qDist(q, qk)

#funkcja opisujaca sile odpychajaca od i-tej przeszkody
def Foi(q, qoi, koi, do):
	if qDist(q, qoi) > do:
		return 0
	if q==qoi:
		return 0
	return -koi*(1/qDist(q, qoi) - 1/do)*(1/(qDist(q, qoi)**2))

#suma sil odpychajacych od przeszkod
def TFoi(q, tp, koi=1, do=40):
	TFp=0
	for p in tp:
		TFp += (Foi(q, p, koi, do))
	return TFp

#funkcja zwracajaca wektor przesuniecia od punktu q do punktu qk
def Vec(q, qk):
	return (q[0]-qk[0], q[1]-qk[1])

x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X**0)

TFp = TFoi (start_point, obst_vect)
TF1 = list()
fmp = list()

n=0
# ------------------------------------------------ zmienne d0 ------------------------------------------------
#generowanie macierzy wartosci sil
for pointy in y:
	for pointx in x:
		n+=1
		zpointy=int(pointy/delta+1/delta*10)
		zpointx=int(pointx/delta+1/delta*10)
		Z[zpointy, zpointx] = TFoi((pointx, pointy), obst_vect, koi=10, do=0.7) + Fp((pointx, pointy), finish_point, kp=0.017)

fig, axs = plt.subplots(1, 3)
axs[0].plot(start_point[0], start_point[1], "or", color='blue')
axs[0].plot(finish_point[0], finish_point[1], "or", color='blue')
for obstacle in obst_vect:
    axs[0].plot(obstacle[0], obstacle[1], "or", color='black')

axs[0].set_title('d0=0.7')
axs[0].imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=1, vmin=-1)

axs[0].grid(True)

#generowanie macierzy wartosci sil
for pointy in y:
	for pointx in x:
		n+=1
		zpointy=int(pointy/delta+1/delta*10)
		zpointx=int(pointx/delta+1/delta*10)
		Z[zpointy, zpointx] = TFoi((pointx, pointy), obst_vect, koi=10, do=2) + Fp((pointx, pointy), finish_point, kp=0.017)

axs[1].plot(start_point[0], start_point[1], "or", color='blue')
axs[1].plot(finish_point[0], finish_point[1], "or", color='blue')
for obstacle in obst_vect:
    axs[1].plot(obstacle[0], obstacle[1], "or", color='black')

axs[1].set_title('d0=2')
axs[1].imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=1, vmin=-1)

axs[1].grid(True)

#generowanie macierzy wartosci sil
for pointy in y:
	for pointx in x:
		n+=1
		zpointy=int(pointy/delta+1/delta*10)
		zpointx=int(pointx/delta+1/delta*10)
		Z[zpointy, zpointx] = TFoi((pointx, pointy), obst_vect, koi=10, do=40) + Fp((pointx, pointy), finish_point, kp=0.017)

axs[2].plot(start_point[0], start_point[1], "or", color='blue')
axs[2].plot(finish_point[0], finish_point[1], "or", color='blue')
for obstacle in obst_vect:
    axs[2].plot(obstacle[0], obstacle[1], "or", color='black')

axs[2].set_title('d0=40')
axs[2].imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=1, vmin=-1)

axs[2].grid(True)
# Widocznie wraz ze wzrostem parametru d0 (odleglosci granicznej oddzialywania przeszkody) zwieksza sie dystans,
# na ktorym widoczne jest dzialanie potencjalu odpychajacego przeszkody. Zmiany te sa najbardziej widoczne dla d0
# z przedzialu od 0 do 2

# ------------------------------------------------ zmienne k0i ------------------------------------------------
#generowanie macierzy wartosci sil
for pointy in y:
	for pointx in x:
		n+=1
		zpointy=int(pointy/delta+1/delta*10)
		zpointx=int(pointx/delta+1/delta*10)
		Z[zpointy, zpointx] = TFoi((pointx, pointy), obst_vect, koi=1, do=40) + Fp((pointx, pointy), finish_point, kp=0.017)

fig1, axs1 = plt.subplots(1, 3)
axs1[0].plot(start_point[0], start_point[1], "or", color='blue')
axs1[0].plot(finish_point[0], finish_point[1], "or", color='blue')
for obstacle in obst_vect:
    axs1[0].plot(obstacle[0], obstacle[1], "or", color='black')

axs1[0].set_title('k0i=1')
axs1[0].imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=1, vmin=-1)

axs1[0].grid(True)

#generowanie macierzy wartosci sil
for pointy in y:
	for pointx in x:
		n+=1
		zpointy=int(pointy/delta+1/delta*10)
		zpointx=int(pointx/delta+1/delta*10)
		Z[zpointy, zpointx] = TFoi((pointx, pointy), obst_vect, koi=10, do=40) + Fp((pointx, pointy), finish_point, kp=0.017)

axs1[1].plot(start_point[0], start_point[1], "or", color='blue')
axs1[1].plot(finish_point[0], finish_point[1], "or", color='blue')
for obstacle in obst_vect:
    axs1[1].plot(obstacle[0], obstacle[1], "or", color='black')

axs1[1].set_title('k0i=10')
axs1[1].imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=1, vmin=-1)

axs1[1].grid(True)

#generowanie macierzy wartosci sil
for pointy in y:
	for pointx in x:
		n+=1
		zpointy=int(pointy/delta+1/delta*10)
		zpointx=int(pointx/delta+1/delta*10)
		Z[zpointy, zpointx] = TFoi((pointx, pointy), obst_vect, koi=40, do=40) + Fp((pointx, pointy), finish_point, kp=0.017)

axs1[2].plot(start_point[0], start_point[1], "or", color='blue')
axs1[2].plot(finish_point[0], finish_point[1], "or", color='blue')
for obstacle in obst_vect:
    axs1[2].plot(obstacle[0], obstacle[1], "or", color='black')

axs1[2].set_title('k0i=40')
axs1[2].imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=1, vmin=-1)

axs1[2].grid(True)
# Widocznie wraz ze wzrostem parametru k0i rosnie potencjal przeszkody, czyli takze jej sila odpychajaca

# ------------------------------------------------ zmienne kp ------------------------------------------------
#generowanie macierzy wartosci sil
for pointy in y:
	for pointx in x:
		n+=1
		zpointy=int(pointy/delta+1/delta*10)
		zpointx=int(pointx/delta+1/delta*10)
		Z[zpointy, zpointx] = TFoi((pointx, pointy), obst_vect, koi=1, do=40) + Fp((pointx, pointy), finish_point, kp=0.01)

fig2, axs2 = plt.subplots(1, 3)
axs2[0].plot(start_point[0], start_point[1], "or", color='blue')
axs2[0].plot(finish_point[0], finish_point[1], "or", color='blue')
for obstacle in obst_vect:
    axs2[0].plot(obstacle[0], obstacle[1], "or", color='black')

axs2[0].set_title('kp=0.01')
axs2[0].imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=1, vmin=-1)

axs2[0].grid(True)

#generowanie macierzy wartosci sil
for pointy in y:
	for pointx in x:
		n+=1
		zpointy=int(pointy/delta+1/delta*10)
		zpointx=int(pointx/delta+1/delta*10)
		Z[zpointy, zpointx] = TFoi((pointx, pointy), obst_vect, koi=1, do=40) + Fp((pointx, pointy), finish_point, kp=0.017)

axs2[1].plot(start_point[0], start_point[1], "or", color='blue')
axs2[1].plot(finish_point[0], finish_point[1], "or", color='blue')
for obstacle in obst_vect:
    axs2[1].plot(obstacle[0], obstacle[1], "or", color='black')

axs2[1].set_title('kp=0.017')
axs2[1].imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=1, vmin=-1)

axs2[1].grid(True)

#generowanie macierzy wartosci sil
for pointy in y:
	for pointx in x:
		n+=1
		zpointy=int(pointy/delta+1/delta*10)
		zpointx=int(pointx/delta+1/delta*10)
		Z[zpointy, zpointx] = TFoi((pointx, pointy), obst_vect, koi=1, do=40) + Fp((pointx, pointy), finish_point, kp=0.03)

axs2[2].plot(start_point[0], start_point[1], "or", color='blue')
axs2[2].plot(finish_point[0], finish_point[1], "or", color='blue')
for obstacle in obst_vect:
    axs2[2].plot(obstacle[0], obstacle[1], "or", color='black')

axs2[2].set_title('kp=0.03')
axs2[2].imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=1, vmin=-1)

axs2[2].grid(True)

plt.show()
# Widocznie wraz ze wzrostem parametru kp rosnie sila przyciagajaca, co widac bo odcieniu dla diagramow