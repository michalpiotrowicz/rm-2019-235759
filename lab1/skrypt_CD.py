import numpy as np
import math as math
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import random as rnd
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from numpy import linalg as la

delta = 0.1
x_size = 10
y_size = 10
start_y=rnd.randint(-10,10)
#losowanie punktu poczatkowego
start_point=(-10,start_y)
finish_y=rnd.randint(-10,10)
# losowanie punktu koncowego
finish_point=(10,finish_y)
# losowanie wspolrzednych 4 przeszkod
obst_vect = [(rnd.randint(-10,10), rnd.randint(-10,10)), (rnd.randint(-10,10), rnd.randint(-10,10)),
			 (rnd.randint(-10,10), rnd.randint(-10,10)), (rnd.randint(-10,10), rnd.randint(-10,10)),
			 (rnd.randint(-10,10), rnd.randint(-10,10)), (rnd.randint(-10,10), rnd.randint(-10,10))]


# funkcja obliczajaca odleglosc miedzy punktami q i qk
def qDist(q: tuple, qk: tuple) -> float:
	return np.sqrt((q[0] - qk[0])**2 + (q[1] - qk[1])**2)


# funkcja opisujaca potencjal przyciagajacy robota w punkcie q
def Up(q: tuple, qk: tuple, kp: float) -> float:
	return 0.5*kp*(qDist(q, qk)**2)


# funkcja opisujaca potencjal odpychakjacy od i-tej przeszkody
def Voi(q: tuple, qoi: tuple, koi: float, do: float) -> float:
	if qDist(q, qoi) > do:
		return 0
	return 0.5*(koi*(1/qDist(q, qoi) - 1/do)**2)


# funkcja opisujaca sile przyciagajaca
def Fp(q: tuple, qk: tuple, kp: float) -> float:
	return kp*qDist(q, qk)


# funkcja opisujaca sile odpychajaca od i-tej przeszkody
def Foi(q: tuple, qoi: tuple, koi: float, do: float) -> float:
	if qDist(q, qoi) > do:
		return 0
	if q==qoi:
		return 0
	return - koi*(1/qDist(q, qoi) - 1/do)*(1/(qDist(q, qoi)**2))


# suma sil odpychajacych od przeszkod
def TFoi(q: tuple, tp: list, koi: float = 1, do: float = 40) -> float:
	TFp=0
	for p in tp:
		TFp += (Foi(q, p, koi, do))
	return TFp


# funkcja zwracajaca wektor przesuniecia od punktu q do punktu qk
def Vec(q: tuple, qk: tuple) -> np.ndarray:
	return np.array([qk[0]-q[0], qk[1]-q[1]])


def nextPos(current: tuple, qk: tuple, obst_vect: list, dr: int = 5, kp: float = 0.04, koi: float = 1, do: float = 40) -> tuple:
	resP = current
	resV = - kp * Vec(current, qk)
	for i in obst_vect:
		if current[0] < 10 and current[1] < 10 and current[1] > -10:
			# ponizszy warunek dotyczy podpunktu D
			if qDist(current, i) < dr:
				resV += - Foi(current, i, koi, do) * Vec(current, i) / qDist(current, i)

	print(resV[0], resV[1])
	print(resP[0], resP[1])
	resA = np.tan(resV[1]/resV[0])
	if -np.pi/8 <= resA < np.pi/8:
		resP = (current[0] + delta, current[1])
	if np.pi/8 <= resA < 3*np.pi/8:
		resP = (current[0] + delta, current[1] + delta)
	if 3*np.pi/8 <= resA < 5*np.pi/8:
		resP = (current[0], current[1] + delta)
	if 5*np.pi/8 <= resA < 7*np.pi/8:
		resP = (current[0] - delta, current[1] + delta)

	if -3*np.pi/8 <= resA < -np.pi/8:
		resP = (current[0] + delta, current[1] - delta)
	if -5*np.pi/8 <= resA < -3*np.pi/8:
		resP = (current[0], current[1] - delta)
	if -7*np.pi/8 <= resA < -5*np.pi/8:
		resP = (current[0] - delta, current[1] - delta)
	if resA < -7*np.pi/8 and resA >= 7*np.pi/8:
		resP = (current[0] - delta, current[1])
	return resP


def Path(q: tuple, qk: tuple, obst_vect: list) -> list:
	steps = list()
	steps.append(q)
	current = q
	i = 0
	while qDist(current, qk) > .5:
		current = nextPos(current, qk, obst_vect)
		steps.append(current)
		i += 1
		if i > 250:
			break
	return steps


x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X**0)

TFp = TFoi (start_point, obst_vect)
TF1 = list()
fmp = list()

n = 0

# generowanie macierzy wartosci sil
for pointy in y:
	for pointx in x:
		n+=1
		zpointy=int(pointy/delta+1/delta*10)
		zpointx=int(pointx/delta+1/delta*10)
		Z[zpointy, zpointx] = TFoi((pointx, pointy), obst_vect, koi=1, do=40) + Fp((pointx, pointy), finish_point, kp=0.04)

fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=1, vmin=-1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

steps = Path(start_point, finish_point, obst_vect)
for p in steps:
	plt.plot(p[0], p[1], "or", color='blue')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
