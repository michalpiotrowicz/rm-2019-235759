import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import random as rnd
from matplotlib.path import Path
from matplotlib.patches import PathPatch

delta = 0.1
x_size = 10
y_size = 10
start_y=rnd.randint(-10,10)
#losowanie punktu poczatkowego
start_point=(-10,start_y)
finish_y=rnd.randint(-10,10)
#losowanie punktu koncowego
finish_point=(10,finish_y)
#losowanie wspolrzednych 4 przeszkod
obst_vect = [(rnd.randint(-10,10), rnd.randint(-10,10)), (rnd.randint(-10,10), rnd.randint(-10,10)),
			 (rnd.randint(-10,10), rnd.randint(-10,10)), (rnd.randint(-10,10), rnd.randint(-10,10))]

#funkcja obliczajaca odleglosc miedzy punktami q i qk
def qDist(q, qk):
	return np.sqrt((q[0] - qk[0])**2 + (q[1] - qk[1])**2)

#funkcja opisujaca potencjal przyciagajacy robota w punkcie q
def Up(q, qk, kp):
	return 0.5*kp*(qDist(q, qk)**2)

#funkcja opisujaca potencjal odpychakjacy od i-tej przeszkody
def Voi(q, qoi, koi, do):
	if qDist(q, qoi) > do:
		return 0
	return 0.5*(koi*(1/qDist(q, qoi) - 1/do)**2)

#funkcja opisujaca sile przyciagajaca
def Fp(q, qk, kp):
	return kp*qDist(q, qk)

#funkcja opisujaca sile odpychajaca od i-tej przeszkody
def Foi(q, qoi, koi, do):
	if qDist(q, qoi) > do:
		return 0
	if q==qoi:
		return 0
	return -koi*(1/qDist(q, qoi) - 1/do)*(1/(qDist(q, qoi)**2))

#suma sil odpychajacych od przeszkod
def TFoi(q, tp, koi=1, do=40):
	TFp=0
	for p in tp:
		TFp += (Foi(q, p, koi, do))
	return TFp

#funkcja zwracajaca wektor przesuniecia od punktu q do punktu qk
def Vec(q, qk):
	return (q[0]-qk[0], q[1]-qk[1])

x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X**0)

TFp = TFoi (start_point, obst_vect)
TF1 = list()
fmp = list()

n=0

#generowanie macierzy wartosci sil
for pointy in y:
	for pointx in x:
		n+=1
		zpointy=int(pointy/delta+1/delta*10)
		zpointx=int(pointx/delta+1/delta*10)
		Z[zpointy, zpointx] = TFoi((pointx, pointy), obst_vect, koi=1, do=40) + Fp((pointx, pointy), finish_point, kp=0.017)

fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=1, vmin=-1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
